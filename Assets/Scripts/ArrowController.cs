﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour {

	SpheresListScript sphereList;

	void Start () 
	{
		sphereList = GameObject.FindObjectOfType<SpheresListScript>();
	}

	Vector3 rotat;

	void Update () 
	{
		rotat = MyCustomVector3.GlobalLookAtRotation(transform.position, sphereList.closestSphere.transform.position); //каждый кадр поворачивается на угол чтобы смотреть на ближайшую сферу
		transform.localEulerAngles = rotat;

	}
}
