﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereColorChangeScript : MonoBehaviour {


	public float distance;

	public Color closeColor = Color.red;
	public Color farColor = Color.green;

	public float lowPriorityDistance; //дистанция, после которой сфера становится низкоприоритетной

	public float highPriorityDistanceCheckDelayMS; //задержка корутины для сфер с высоким приоритетом (находятся близко)
	public float lowPriorityDistanceCheckDelayMS; //для дальних сфер с низким приоритетом

	bool isLowPriorityMode;



	MeshRenderer render;
	SpheresListScript sphereList;

	IEnumerator GetDistanceToCameraAndChangeColor() //измерение дистанции и изменение цвета
	{
		while(true)
		{
			distance = MyCustomVector3.MyDistance(transform.position, Camera.main.transform.position);
			if(distance > lowPriorityDistance) {isLowPriorityMode = true;} else {isLowPriorityMode = false;} //проверка на низкий и высокий приоритеты, от приоритета зависит частота измерения дистанции
			if(!isLowPriorityMode)
			{
				render.material.color = Color.Lerp(closeColor, farColor, distance/lowPriorityDistance);
				yield return new WaitForSeconds(highPriorityDistanceCheckDelayMS);
			}
			else 
			{
				render.material.color = farColor;
				yield return new WaitForSeconds(lowPriorityDistanceCheckDelayMS);
			}
		}
	}



	void Start () 
	{
		sphereList = GameObject.FindObjectOfType<SpheresListScript>();
		highPriorityDistanceCheckDelayMS /= 1000f;
		lowPriorityDistanceCheckDelayMS /= 1000f;
		render = GetComponent<MeshRenderer>();
		StartCoroutine("GetDistanceToCameraAndChangeColor");
	}



}
