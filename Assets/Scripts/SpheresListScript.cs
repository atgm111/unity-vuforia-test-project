﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpheresListScript : MonoBehaviour {

	public float checkForClosestDelayMS;

	public GameObject closestSphere;
	public List<GameObject> spheresList;

	public float minDistance = float.MaxValue;

	IEnumerator FindClosest() //нахождение сферы с минимальной дистанцией до камеры
	{
		while(true)
		{
			foreach(GameObject gObj in spheresList)
			{
				if(gObj.GetComponent<SphereColorChangeScript>().distance < minDistance)
				{
					minDistance = gObj.GetComponent<SphereColorChangeScript>().distance;
					closestSphere = gObj;
				}
			}
			yield return new WaitForSeconds(checkForClosestDelayMS);
			minDistance = float.MaxValue;
		}
	}

	void Start()
	{
		StartCoroutine("FindClosest");
	}
}
