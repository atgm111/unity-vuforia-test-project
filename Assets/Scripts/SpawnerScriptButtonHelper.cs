﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpawnerScriptButtonHelper : MonoBehaviour {

	public GameObject inputFieldTextInt;

	public void RespawnSpheresIntFromField()
	{
		GetComponent<SphereSpawnerScript>().RespawnSpheres(int.Parse(inputFieldTextInt.GetComponent<Text>().text));
	}

}
