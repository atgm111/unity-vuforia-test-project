﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SphereSpawnerScript : MonoBehaviour {

	public GameObject spherePrefab;
	public GameObject spheresParentGameObject;

	public int defaultSpheresCount;

	public float minRandomXZ; //границы для сфер
	public float maxRandomXZ;
	public float minRandomY;
	public float maxRandomY;

	List<GameObject> spheresList;


	void Start () 
	{
		spheresList = GameObject.FindObjectOfType<SpheresListScript>().spheresList;
		RespawnSpheres(defaultSpheresCount);
	}

	public void RespawnSpheres(int spheresCount) //проверка количества сфер и создание дополнительных/удаление лишних
	{
		if (spheresList.Count < spheresCount) 
		{
			SpawnSpheres(spheresCount - spheresList.Count);
		}
		else
		{
			DispawnSpheres(spheresList.Count - spheresCount);
		}
	}

	void SpawnSpheres(int spheresCount)
	{
		StartCoroutine("SpawnSpheresIEnum", spheresCount);
	}

	IEnumerator SpawnSpheresIEnum(int spheresCount) //инициализация сфер и добавление в лист (1 сфера за 1 кадр)
	{
		for (int i = 0; i < spheresCount; i++)
		{
			GameObject sphere = Instantiate(spherePrefab, new Vector3(Random.Range(minRandomXZ, maxRandomXZ), Random.Range(minRandomY, maxRandomY), Random.Range(minRandomXZ, maxRandomXZ)), Quaternion.identity);
			sphere.transform.parent = spheresParentGameObject.transform;
			spheresList.Add(sphere);
			yield return new WaitForEndOfFrame(); 
		}
	}

	void DispawnSpheres(int spheresCount)
	{
		StartCoroutine("DispawnSpheresIEnum", spheresCount);
	}

	IEnumerator DispawnSpheresIEnum(int spheresCount) //удаление сфер из сцены и из листа (1 сфера за 1 кадр)
	{
		for (int i = 0; i < spheresCount; i++)
		{
			Destroy(spheresList[spheresList.Count - 1]);
			spheresList.RemoveAt(spheresList.Count - 1);
			yield return new WaitForEndOfFrame();
		}
	}


}
