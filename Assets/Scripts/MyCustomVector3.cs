﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct MyCustomVector3 {

	public static float MyDistance(Vector3 posA, Vector3 posB)
	{
		return Mathf.Sqrt( Mathf.Pow((posB.x - posA.x), 2f) + Mathf.Pow((posB.y - posA.y), 2f) + Mathf.Pow((posB.z - posA.z), 2f) ); //вычисление расстояния между двумя точками в пространстве
	}



	public static Vector3 GlobalLookAtRotation(Vector3 posA, Vector3 posB) //угол на цель
	{
		Vector3 rotation;

		float xCath; 
		float yCath;
		float zCath;

		xCath = posB.x - posA.x; //вычисление катетов
		yCath = posB.y - posA.y;
		zCath = posB.z - posA.z;

		rotation.z = Mathf.Atan2(yCath, xCath) * Mathf.Rad2Deg; //нахождение угла между двумя точками через тангенс двух катетов
		rotation.x = 0f;
		rotation.y = 0f;

		return rotation;
	}

}
